all: done/2014-06 done/2015-03 done

done:
	mkdir -p done

done/2014-06: done
	cd 2014-06; occam build base ubuntu-base-2014-06
	touch done/2014-06

done/2015-03: done
	cd 2015-03; occam build base ubuntu-base-2015-03
	touch done/2015-03
